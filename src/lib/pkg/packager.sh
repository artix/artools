#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

get_packager_name() {
    local packager=$1
    local packager_pattern="(.+) <(.+@.+)>"
    local name

    if [[ ! $packager =~ $packager_pattern ]]; then
        return 1
    fi

    name=$(echo "${packager}"|sed -E "s/${packager_pattern}/\1/")
    printf "%s" "${name}"
}

get_packager_email() {
    local packager=$1
    local packager_pattern="(.+) <(.+@.+)>"
    local email

    if [[ ! $packager =~ $packager_pattern ]]; then
        return 1
    fi

    email=$(echo "${packager}"|sed -E "s/${packager_pattern}/\2/")
    printf "%s" "${email}"
}

is_packager_name_valid() {
    local packager_name=$1
    if [[ -z ${packager_name} ]]; then
        return 1
    elif [[ ${packager_name} == "John Tux" ]]; then
        return 1
    elif [[ ${packager_name} == "Unknown Packager" ]]; then
        return 1
    fi
    return 0
}

is_packager_email_official() {
    local packager_email=$1
    if [[ -z ${packager_email} ]]; then
        return 1
    elif [[ $packager_email =~ .+@artixlinux.org ]]; then
        return 0
    fi
    return 1
}

# shellcheck source=config/makepkg/x86_64.conf
load_makepkg_config
