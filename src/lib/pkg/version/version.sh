#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_VERSION_SH:-} ]] || return 0
ARTOOLS_INCLUDE_VERSION_SH=1

set -e


artixpkg_version_usage() {
    COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS]

    Shows the current version information of artixpkg

    OPTIONS
        -h, --help    Show this help text
_EOF_
}

artixpkg_version_print() {
    cat <<- _EOF_
    artixpkg @buildtoolver@
_EOF_
}

artixpkg_version() {
    while (( $# )); do
        case $1 in
            -h|--help)
                artixpkg_version_usage
                exit 0
            ;;
            *)
                die "invalid argument: %s" "$1"
            ;;
        esac
    done

    artixpkg_version_print
}
